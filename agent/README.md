docker run -it -v $PWD:/workspace golang:1.20 bash
cd /workspace
go mod tidy
CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -tags netgo -ldflags '-w -extldflags "-static"' -o kiotop-agent ./cmd/main/