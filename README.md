# kiotop



## TL;DR;

This admin toolbox allows to perform pseudo synchronized iotop command on each nodes of multiple Kubernetes clusters to provide Disk Read/Write dataflow at:
- [ ] Cloud view level
- [ ] Cluster view level
- [ ] Node view level
- [ ] Thread view level

## Architecture

```mermaid
flowchart LR
    cloudcontroller((super-controller))
    clouddb((global-database))
    subgraph clusterk [Cluster k]
        controllerk(controller k)
        dbk(local-database k)
        subgraph nodei [Node i]
            ai(agent i)
        end
        subgraph nodeip1 [Node i+1]
            aip1(agent i+1)
        end
    end
    subgraph clusterkp1 [Cluster k+1]
        controllerkp1(controller k+1)
        dbkp1(local-database k+1)
    end
    controllerk -."update/check scan request and get reports ".-> dbk
    ai-- "check/update scan request" -->dbk
    ai-. "write iotop report" .->dbk
    aip1-- "check/update scan request" -->dbk
    aip1-. "write iotop report" .->dbk
    controllerk --"check scan request"--> cloudcontroller
    controllerk -."send cluster-scoped report ".-> cloudcontroller
    controllerkp1 --> cloudcontroller
    controllerkp1 -.-> cloudcontroller
    controllerkp1 -.-> dbkp1
    cloudcontroller -."read/write global reports".-> clouddb
```


## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
